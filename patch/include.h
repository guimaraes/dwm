/* Bar functionality */
#include "bar_indicators.h"
#include "bar_tagicons.h"
#include "bar.h"

#include "bar_ltsymbol.h"
#include "bar_status.h"
#include "bar_tags.h"
#include "bar_wintitle.h"
#include "bar_awesomebar.h"
#include "bar_systray.h"
#include "bar_wintitleactions.h"

/* Other patches */
#include "attachx.h"
#include "autostart.h"
#include "cfacts.h"
#include "dragcfact.h"
#include "dragmfact.h"
#include "focusurgent.h"
#include "pertag.h"
#include "push_no_master.h"
#include "restartsig.h"
#include "vanitygaps.h"
#include "zoomswap.h"
#include "xrdb.h"
/* Layouts */
#include "layout_monocle.h"
#include "layout_tile.h"

